﻿using UnityEngine;
using UnityEngine.EventSystems;

public static class InitializationManager
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static void Init()
    {
        var virtualKeys = Object.FindObjectsOfType<VirtualKey>();
        var inputField = Object.FindObjectOfType<MyInputField>();
        
        inputField.InsertVirtualKeys(virtualKeys);
        
        EventSystem.current.SetSelectedGameObject(inputField.gameObject);
    }
}