using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyInputField : InputFieldOriginal
{
    //From https://www.reddit.com/r/Unity3D/comments/94z2aa/easy_capslock_detection/
    [System.Runtime.InteropServices.DllImport("USER32.dll")] 
    private static extern short GetKeyState(int nVirtKey);
    private bool IsCapsLockOn => (GetKeyState(0x14) & 1) > 0;
    
    public void InsertVirtualKeys(VirtualKey[] keys)
    {
        for (int i = 0; i < keys.Length; i++)
        {
            var virtualKey = keys[i];
            virtualKey.OnButtonClick += VirtualButtonClickHandler;
        }
    }

    private void VirtualButtonClickHandler(KeyCode keyCode)
    {
        Event customEvent = new Event();
        
        customEvent.command = Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand);
        customEvent.control = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
        customEvent.alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
        customEvent.shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        customEvent.capsLock = IsCapsLockOn;

        if (char.TryParse(keyCode.ToString(), out var inputChar))
        {
            var upperCase = customEvent.shift;
            if (customEvent.capsLock) upperCase = !upperCase;
            if (!upperCase) inputChar = char.ToLower(inputChar);
            
            customEvent.character = inputChar;
        }
        else
        {
            customEvent.keyCode = keyCode;
        }

        customEvent.type = EventType.KeyUp;
        customEvent.clickCount = 1;

        KeyPressed(customEvent);
        UpdateLabel();
    }
    
    public override void OnDeselect(BaseEventData eventData)
    {
        StartCoroutine(Refocus(caretPositionInternal, caretSelectPositionInternal));
    }
    
    private IEnumerator Refocus(int position, int selectPosition)
    {
        yield return 0;
        EventSystem.current.SetSelectedGameObject(gameObject);
        caretPositionInternal = position;
        caretSelectPositionInternal = selectPosition;
        
        UpdateLabel();
    }
}