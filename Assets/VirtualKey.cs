using System;
using UnityEngine;
using UnityEngine.UI;

public class VirtualKey : MonoBehaviour
{
    [SerializeField] private KeyCode keyCode;

    public event Action<KeyCode> OnButtonClick; 

    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ButtonClickHandler);
    }

    private void ButtonClickHandler()
    {
        OnButtonClick?.Invoke(keyCode);
    }
}